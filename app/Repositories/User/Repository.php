<?php

namespace App\Repositories\User;

use App\Repositories\User\Iface;
use App\Models\User as Model;

class Repository implements Iface
{
    public function get(int $id) : ?Model
    {
        return Model::find($id);
    }
}