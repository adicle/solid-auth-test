<?php

namespace App\Repositories\User;

use App\Models\User as Model;

interface Iface
{
    public function get(int $id) : ?Model;
}