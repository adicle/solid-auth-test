<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Modules\Auth\Module::class, function () {
            return new \App\Modules\Auth\Providers\LaravelAuth;
        });
    }
}