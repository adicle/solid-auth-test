<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register()
    {
        $repos = [
            [
                \App\Repositories\User\Iface::class,
                \App\Repositories\User\Repository::class,
            ]
        ];

        foreach ($repos as $repo) {
            $this->app->bind($repo[0], $repo[1]);
        }
    }
}