<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\RedirectResponse as Redirect;

use App\Http\Requests\Login as Request;
use App\Modules\Auth\Module as Module;

class LoginController extends Controller
{
    protected $module;

    public function __construct(Module $module)
    {
        $this->module = $module;
    }

    public function index() : View
    {
        return view('login');
    }

    public function store(Request $request) : Redirect
    {
        $attempt = $this->module->attempt($request);

        return ($attempt) ? redirect()->route('dashboard') : back();
    }
}
