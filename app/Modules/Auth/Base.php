<?php

namespace App\Modules\Auth;

class Base
{
    protected $user;

    public function __construct()
    {
        $this->user = resolve(\App\Repositories\User\Iface::class);
    }
}