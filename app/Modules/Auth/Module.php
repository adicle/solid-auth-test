<?php

namespace App\Modules\Auth;

use App\Http\Requests\Login as Request;

interface Module
{
    public function attempt(Request $request);
}