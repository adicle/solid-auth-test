<?php

namespace App\Modules\Auth\Providers;

use Illuminate\Support\Facades\Auth;

use App\Modules\Auth\Module;
use App\Http\Requests\Login as Request;
use App\Modules\Auth\Base;

class LaravelAuth extends Base implements Module
{
    public function attempt(Request $request) : bool
    {
        $credentials = $request->only('email', 'password');

        return Auth::attempt($credentials);
    }
}